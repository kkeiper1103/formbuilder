<?php 

namespace FormBuilder\HTML;
/**
 * Adapted from link below
 */
 
#http://davidwalsh.name/create-html-elements-php-htmlelement-class

class Tag{
	
	private $type;
	private $attr = array();
	private $self_closing = array();
	
	public function __construct( $type, $self_closing = array('input', 'img', 'hr', 'br', 'meta', 'link') )
	{
		$this->type = $type;
		$this->self_closing = $self_closing;
		
		return $this;
	}
	
	public function get( $attr )
	{
		return $this->attr[$attr];
	}
	
	public function set( $key, $value = "" )
	{
		if( is_array($key) )
		{
			$this->attr = array_merge($this->attr, $key);
		}
		else
		{
			$this->attr[$key] = $value;
		}
		
		return $this;
	}
	
	public function remove($attr)
	{
		if( isset( $this->attr[$attr] ) )
		{
			unset( $this->attr[$attr] );
		}
		
		return $this;
	}
	
	public function clear()
	{
		$this->attr = array();
		
		return $this;
	}
	
	public function render()
	{
		$element = array();
		
		$element[] = "<";
		$element[] = $this->type;
		
		if( sizeof($this->attr) > 0 )
		{
			foreach( $this->attr as $key => $value )
			{
				if( strcmp("text", $key) !== 0 )
				{
					$element[] = " {$key}='{$value}'";
				}
			}
		}
		
		if( ! in_array( $this->type, $this->self_closing ) )
		{
			$element[] = ">{$this->attr['text']}</{$this->type}>";
		}
		else
		{
			$element[] = "/>";
		}
		
		return implode('', $element);
	}
	
	public function __toString()
	{
		return $this->render();
	}
	
	// static function for 5.3 <= for chainability
	// usage: Tag::Create("p")->set(array());
	
	// in 5.4, you can bypass with:
	// (new Tag("p"))->set(array());
	
	// but I like the 5.3 style one better... :P
	
	public static function Create( $type, $self_closing = array('input', 'img', 'hr', 'br', 'meta', 'link') )
	{
		return new Tag( $type, $self_closing );
	}
	
}
