<?php

namespace FormBuilder;

use FormBuilder\HTML\Tag;

class FormBuilder{
	
	protected $model = null;
	protected $form_name;
	protected $form;
	
	protected $is_multipart = false;
	
	protected $action = "";
	protected $method = "post";
	
	protected $errors = array();
	
	protected $reflection;
	
	public function __construct( $model, $callback )
	{
		
		$this->model = $model;
		$this->form_name = strtolower(get_class($model));
		
		//$this->reflection = new ReflectionObject( $model );
		
		ob_start();
			$callback($this);
		$form_contents = ob_get_contents();
		ob_end_clean();
		
		// render form based on enctype
		$enctype = ($this->is_multipart) ? "multipart/form-data" : "application/x-www-form-urlencoded";
		
        // guess form if no callback was provided
        if( strlen($form_contents) == 0 || empty($form_contents) )
        {
            $form_contents = $this->guess_form();
        }
        
		$this->form = new Tag("form");
		$this->form->set(array(
			"id" => "{$this->form_name}_form",
			'name' => $this->form_name,
			'enctype' => $enctype,
			'action' => $this->action,
			'method' => $this->method,
			'text' => $form_contents
		));
		
		echo $this->form;
		
	}
	
	public function fields_for( $nested_model, $callback ){
		
		// TODO: Implement subform
		
		$callback($this);
	}
	
	public function label( $field, $text, $attributes = array() ){
		//if( $this->reflection->hasProperty($field) )
		//{
			return Tag::Create("label")
				->set(array(
					'for' => "{$this->form_name}_{$field}",
					'text' => $text
				))
				->set($attributes);
		//}
	}
	public function button( $field, $content, $attributes = array() ){
		
		return Tag::Create("button")
			->set('text', $content)
			->set( $this->get_name_and_id($field) )
			->set($attributes);
		
	}
	public function check_box( $field, $attributes = array() ){
		
		return Tag::Create("input")
			->set("type", 'checkbox')
			->set( $this->get_name_and_id($field) )
			->set($attributes);
	}
	public function collection_select( $field, $collection ){
		
		$options = $this->options_for_select($collection);
		
		return Tag::Create("select")->set('text', $options)->set( $this->get_name_and_id($field) );
	}
	
	public function options_for_select($collection)
	{
		$options = array();
		foreach($collection as $option)
		{
			$options[ $option[0] ] = Tag::Create("option")->set(array(
				'text' => $option[1],
				'value' => $option[0]
			))->render();
		}
		
		return implode("\n", $options);
	}
	
	public function date_select( $field, $date, $attributes = array() ){
		
		if( is_null($date) )
			$date = $this->value($date);
		
		$value = date( "Y-m-d", strtotime($date) );
		
		return Tag::Create("input")
			->set(array(
				'type' => 'date',
				'value' => $value
			))
			->set( $this->get_name_and_id($field) )
			->set($attributes);
		
	}
	public function datetime_select( $field, $date, $attributes ){
			
		if( is_null($date) )
			$date = $this->value($date);
		
		$value = date( "Y-m-d H:i:s", strtotime($date) );
		
		return Tag::Create("input")
			->set(array(
				'type' => 'datetime',
				'value' => $value
			))
			->set( $this->get_name_and_id($field) )
			->set($attributes);
		
	}
	public function file_field( $field, $attributes = array() ){
			
		$this->is_multipart = true;
		
		return Tag::Create("input")
			->set("type", "file")
			->set( $this->get_name_and_id($field) )
			->set($attributes);
		
	}
	public function grouped_collection_select( $field ){
		
		throw new \FormBuilder\Exceptions\FieldNotImplementedException("grouped_collection_select");
		
	}
	public function hidden_field( $field, $value = null, $attributes = array()){
		
		if(is_null($value))
			$value = $this->value($field);
		
		return Tag::Create("input")
			->set(array(
				'type' => 'hidden',
				'value' => $value
			))
			->set( $this->get_name_and_id($field) )
			->set($attributes);
		
	}
	public function radio_button( $field, $value, $attributes = array() ){
		
		return Tag::Create("input")
			->set(array(
				'type' => 'radio',
				'value' => $value
			))
			->set( $this->get_name_and_id( $field ) )
			->set($attributes);
		
	}
	public function select( $field, $collection, $attributes = array() ){
			
		$options = $this->options_for_select($collection);
		
		return Tag::Create("select")
			->set('text', $options)
			->set( $this->get_name_and_id($field) )
			->set($attributes);
		
	}
	public function submit( $text, $attributes = array() ){
		
		return Tag::Create("input")
			->set(array(
				'type' => 'submit',
				'value' => $text
			))
			->set( $this->get_name_and_id($field) )
			->set($attributes);
			
	}
	public function text_field( $field, $value, $attributes = array() ){
		
		if( is_null($value) )
			$value = $this->value($field);
			
		return Tag::Create("input")
			->set(array(
				'type' => 'text',
				'value' => $value
			))
			->set( $this->get_name_and_id($field) )
			->set( $attributes );
		
	}
	public function text_area( $field, $value, $attributes = array() ){
				
		if( is_null($value) )
			$value = $this->value($field);
			
		return Tag::Create("textarea")
			->set($this->get_name_and_id($field))
			->set("text", $value)
			->set($attributes);
		
	}
	
	public function time_select( $field, $time, $attributes ){
			
		if( is_null($value) )
			$time = $this->value($field);
		
		$value = date( "H:i:s", strtotime($time) );
		
		return Tag::Create("input")
			->set(array(
				"type" => "time",
				"value" => $time
			))
			->set( $this->get_name_and_id($field) )
			->set( $attributes );
		
	}
	
	public function time_size_select( $field ){
		
		throw new \FormBuilder\Exceptions\FieldNotImplemented("time_size_select");
		
	}
	
    /**
     * @method guess_form
     * 
     * returns a published form guessing the structure based on the information in the model... FUN
     * 
     * TODO: Hook up with ORM Model in Laravel to enable autodetect of Radio Buttons, Selects, etc.
     */
    
    public function guess_form( $field_format = "<p>%s<br />%s</p>" )
    {
        $ref = new \ReflectionClass( $this->model );
        
        $parts = array();
        
        foreach( $ref->getProperties() as $prop )
        {
            $parts[] = $prop;
        }
        
        /**
         * loop through properties and guess the type of field
         */
        
        $fields = array();
        
        foreach( $parts as $field )
        {
            $fields[] = array(
                'label' => $this->label($field->name, ucwords(str_replace("_", " ", $field->name)) ),
                'input' => $this->guess_type($field->name)
            );
            
        }
        
        // lastly, add submit button
        
        $fields[] = array(
            'label' => '',
            'input' => $this->submit("Save")
        );
        
        $ret = array();
        foreach( $fields as $f )
        {
            $ret[] = sprintf( $field_format, $f['label'], $f['input'] );
        }
        
        
        
        return implode("", $ret);
    }
    
	public function to_model(){
		return $this->model;
	}
	
	public function error_messages()
	{
		foreach($this->errors as $err => $msg)
		{
			$this->errors[$err] = "<p>{$msg}</p>";
		}
		
		return implode("\n", $this->errors);
	}
	
	// ************ PROTECTED METHODS ************** //
	
	protected function guess_type( $field )
    {
        
        $field_type = 'text_field';
        
        switch( gettype($this->model->$field) )
        {
            case "integer":
            case "double":
                $field_type = "text_field"; break;
            case "boolean":
                $field_type = "check_box"; break;
            case "string":
                $field_type = ( strlen($this->model->$field) < 50 ) ? "text_field" : "text_area"; break;
        }
        
        return $this->$field_type( $field );
    }
	
	protected function value( $field )
	{
		$input = $_REQUEST[$this->form_name];
		if( isset($input[$field]) && ! empty($input[$field]) )
		{
			return $input[$field];
		}
		else
		{
			return $this->model->$field;
		}
	}
	
	protected function get_name_and_id( $field )
	{
		return array(
			"id" => "{$this->form_name}_{$field}",
			"name" => "{$this->form_name}[{$field}]"
		);
	}
	
	protected function queue_error($msg)
	{
		$this->errors[] = $msg;
	}
}
