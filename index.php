<?php

class Person{
	
	public $id;
	public $name;
	public $last_name;
	public $age;
	public $title;
	public $biography;
	
	public $avatar;
	
    public function setName( $name )
    {
        $this->name = $name;
    }
    
}

$kyle = new Person();

$kyle->active = true;
$kyle->id = "00001";
$kyle->name = "Kyle";
$kyle->last_name = "Schmoe";
$kyle->age = 20;
$kyle->title = "Web Programmer";
$kyle->biography = <<<eob
Kyle is an aspiring web programmer who wants to promote best practices among web developers.
He is a firm believer in the power of using Object Oriented Programming. He is currently 20 
years old, and works at WRL Advertising, Inc.
eob;
$kyle->avatar = "public/images/avatar.png";

spl_autoload_register(function($className){
	$className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
});

?>
<!DOCTYPE html>
<html>
	<head>
		<title>FormBuilder Test</title>
		
	</head>
	<body>
		<?php 
		/*
		new FormBuilder\FormBuilder($kyle, function($p){ ?>
			
			<div class='errors'>
				<?= $p->error_messages(); ?>
			</div>
			
			<?= $p->hidden_field("id"); ?>
			
			<p>
				<?= $p->label("name", "Name"); ?><br />
				<?= $p->text_field("name"); ?>
			</p>
			
			<p>
				<?= $p->label("last_name", "Last Name"); ?><br />
				<?= $p->text_field("last_name"); ?>
			</p>
			
			<?php
			$places = array(
				array('home', "Home"),
				array('work', "Work"),
				array('school', "School"),
				array('church', "Church"),
				array('other', "Other")
			);
			?>
			
			<p>
				<?= $p->label("places", "Places"); ?><br />
				<?= $p->select("places", $places); ?>
			</p>
			
			<p>
				<?= $p->label("age", "Age"); ?><br />
				<?= $p->text_field("age"); ?>
			</p>
			
			<p>
				<?= $p->label("title", "Job Title"); ?><br />
				<?= $p->text_field("title"); ?>
			</p>
			
			<p>
				<?= $p->label("biography", "Biography"); ?><br />
				<?= $p->text_area("biography"); ?>
			</p>
			
			<p>
				<?= $p->label("avatar", "Avatar"); ?><br />
				<?= $p->file_field("avatar"); ?>
			</p>
			
			<p>
				<?= $p->button("btn-custom", "My BUTTON"); ?>
			</p>
			
			<p>
				<?= $p->submit("Save Person"); ?>
			</p>
			
		<?php }); */?>
		
		<?= new FormBuilder\FormBuilder($kyle, function(){}) ?>
		
	</body>
</html>